require "socket"
require "thread"

class Server
	def initialize
		@port = 6000
	    @server = TCPServer.open( @port )
		
		#@rooms = Hash.new
		#Key: room_ref, Value: room_name
		#@clients = Hash.new
		#Key: client_name, Value: client connection
		#@connections = Hash.new{|hash, key| hash[key] = Array.new}
		#Key: room_ref, Value: client_name
		
		#@join_id = 0
		#@room_ref_index = 0
				
		run
	end
	
	def run
		#Initialises thread pool queue						
		worker_q = Queue.new

		#Listening loop	
		#loop{
		server_thread = Thread.new{

			#loop{
				puts "Listening for new clients.."
				while new_client = @server.accept
					worker_q.push new_client
				end
			#}
		}
		
		#Thread pool loop
		#Initialises set number of threads that can process client requests
		#While there are threads left, creates new thread to handle individual clients
		workers = (0...4).map do
		
			worker_thread = Thread.new do
				begin
					#While there are objects in the queue, 
					#the next free thread will pop a client socket off the queue 
					#to be handled 
					while client = worker_q.pop
						puts("Client being handled by thread ##{Thread.current.object_id}.")
						
						#Loop for parsing commands from client and responding
						loop {
							
							while test_string = client.gets(4)
								#puts test_string
								
										
								if test_string == 'OPEN'
									
									string = client.gets(512)
									puts("Request received: #{test_string}#{string.chomp}")
									str_split = string.split(/([:]+)/)
									filename = str_split[2].chomp
									
									#Opens existing file on machine 
									#and sends contents to client proxy
									rfile = open(File.expand_path("../ds/#{filename}"))
									fileContent = rfile.read
									puts "File content: #{fileContent}"
									client.puts(fileContent)
									rfile.close
																		
									
								elsif test_string == 'CLOS'
									
									string = client.gets(512)
									puts("Request received: #{test_string}#{string.chomp}")
									str_split = string.split(/([:]+)/)
									filename2 = str_split[2].chomp
									#puts filename
																		
									#Replaces existing file contents on machine
									data2 = client.gets(2048)
									#puts "New file content received: #{data2}"
									
									destFile = open(File.expand_path("../ds/#{filename2}"), "w+")
																		
									destFile.puts data2
									destFile.close
									
									destFile2 = File.open(File.expand_path("../ds/#{filename2}"))
									newFileContent = destFile2.read
									puts "New file content: #{newFileContent}"
									destFile2.close
																	
								
								else
									puts "Error"
									client.puts "ERROR_CODE: 1\nERROR_DESCRIPTION: Command not valid"
									#client.close	
									#Thread.kill(worker_thread)					
								end
						
						
						
						
							#Command that waits for next recv
							end
						}

					end
					rescue ThreadError
				end

			end
			
		end; "ok"	

		#Joins threads back to main thread before program exits
		workers.map(&:join); "ok"

	end
end



#Takes in port number from command line
#port = ARGV[0]

#server = Server.new(port)

server = Server.new