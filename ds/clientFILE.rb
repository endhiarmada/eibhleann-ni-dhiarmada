require 'socket'

#Opens socket connection
hostname = "localhost"
port = 5000

s = TCPSocket.open(hostname, port)

#Open file request
s.puts( "OPEN:file1.txt" )

#Acknowledgement message
data = s.recv(512)
puts data
sleep(4)


#Read file request and return
s.puts( "READ:file1.txt" )
data = s.recv(2048)
puts "File content received: #{data}"


#Write file request and acknowledgement
s.puts( "WRITE:file1.txt\\nEDIT:How are you?" )
data = s.recv(2048)
puts data


#Close file request and acknowledgement
s.puts( "CLOSE:file1.txt" )
closed = s.recv(512)
puts closed
