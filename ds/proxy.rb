require "socket"
require "thread"

class Server
	def initialize
		@port = 5000
	    @server = TCPServer.open( @port )
				
		@filename = ""
		@folder = ""
		@locked = 1
				
		run
	end
	
	def run
		#Initialises thread pool queue						
		worker_q = Queue.new

		#Listening loop	
		#loop{
		server_thread = Thread.new{

			#loop{
				puts "Listening for new clients.."
				while new_client = @server.accept
					worker_q.push new_client
				end
			#}
		}
		
		#Thread pool loop
		#Initialises set number of threads that can process client requests
		#While there are threads left, creates new thread to handle individual clients
		workers = (0...4).map do
		
			worker_thread = Thread.new do
				begin
					#While there are objects in the queue, 
					#the next free thread will pop a client socket off the queue 
					#to be handled 
					while client = worker_q.pop
						puts("Client being handled by thread ##{Thread.current.object_id}.")
						
						#Loop for parsing commands from client and responding
						loop {
							
							while test_string = client.gets(4)
								#puts test_string
								
								if test_string == 'HELO'
									string = client.gets(512)
									puts("Command received: #{test_string}#{string.chomp}\n")
									client.puts("#{test_string}#{string.chomp}\nIP:[macneill@scss.tcd.ie]\nPort:[#{@port}]\nStudentID:[10315881]")
									#Thread.kill(worker_thread)
								
							
								elsif test_string == 'KILL'
									string = client.gets(512)
									puts "Server being shut down."
									client.close
									exit
							
							
								elsif test_string == 'OPEN'
									
									string = client.gets(512)
									puts("Request received: #{test_string}#{string.chomp}")
									str_split = string.split(/([:]+)/)
									@filename = str_split[2].chomp
									#puts "Filename: #{filename}"
									
									#Requests server(folder) where file is stored
									#from remote directory service
									s_file_thread = Thread.new{
										#puts "In s_file thread"
										hostname = "localhost"
										p = 4000
										d_file = TCPSocket.open(hostname, p)
										
										d_file.puts("FIND:#{@filename}")
										@folder = d_file.recv(2048).chomp
										#puts "Folder: #{@folder}"
																			
									}
																		
									
									#Requests file to be unlocked
									#Blocks until request returns
									l_file_thread = Thread.new{
										#puts "In s_file thread"
										hostname = "localhost"
										p = 3000
										l_file = TCPSocket.open(hostname, p)
										
										l_file.puts("OPEN:#{@filename}")
										lock = l_file.recv(512)
										puts lock
										@locked = 0
																			
									}
									sleep(1)										
									while @locked !=0
										puts "File locked"
										sleep(1)
									end
									
									
									#Requests file from remote service
									s_file_thread = Thread.new{
										#puts "In s_file thread"
										hostname = "localhost"
										p = 6000
										s_file = TCPSocket.open(hostname, p)
										
										
										#Downloads file from remote server 
										#and caches file at client proxy
										#puts "OPEN:#{@filename}"
										s_file.puts("OPEN:#{@folder}/#{@filename}")
										test_data = s_file.recv(2048)
										puts "File content received: #{test_data}"
										destFile = File.open("#{@filename}", "w")
										destFile.puts test_data
										destFile.close
									
									}
																		
									client.puts( "#{@filename} opened" )
									
									
									elsif test_string == 'READ'
									
									string = client.gets(512)
									puts("Request received: #{test_string}#{string.chomp}")
									str_split = string.split(/([:]+)/)
									filename = str_split[2].chomp
									#puts filename
									
									#Reads file contents and displays to client
									rfile = open(File.expand_path("../ds/#{filename}"))
									fileContent = rfile.read
									#puts "File Content: #{fileContent}"
									client.puts(fileContent)
									rfile.close
									
									
									elsif test_string == 'WRIT'
									
									string = client.gets(512)
									puts("Request received: #{test_string}#{string.chomp}")
									str_split = string.split(/([:]+)/)
									a = str_split[2].split(/\\n/)
									filename = a[0]
									#puts "Filename: #{filename}"
									
									edit = str_split[4].chomp
									#puts "Edit: #{edit}"
									
									#Appends existing file with edit
									destFile = open(File.expand_path("../ds/#{filename}"), "a+")
																		
									destFile.puts edit
									destFile.close
									
									#Reads edited doc and displays to client
									destFile2 = File.open(File.expand_path("../ds/#{filename}"))
									newFileContent = destFile2.read
									#puts( "Edited file content: #{newFileContent}" )
									client.puts( "Edited file content: #{newFileContent}" )
									destFile2.close
									
									
								elsif test_string == 'CLOS'
									
									string = client.gets(512)
									puts("Request received: #{test_string}#{string.chomp}")
									str_split = string.split(/([:]+)/)
									@filename = str_split[2].chomp
									#puts @filename
									
									#Requests server(folder) where file is stored
									#from remote directory service
									s_file_thread = Thread.new{
										#puts "In s_file thread"
										hostname = "localhost"
										p = 4000
										d_file = TCPSocket.open(hostname, p)
										
										d_file.puts("FIND:#{@filename}")
										@folder = d_file.recv(2048).chomp
										#puts "Folder: #{@folder}"
																			
									}
									
									#@folder = "file_server1"
									sleep(1)
																		
									
									#Uploads file back to service and deletes from cache
									s_file_thread = Thread.new{
										#puts "In s_file thread"
										hostname = "localhost"
										s_file = TCPSocket.open(hostname, 6000)
										
										s_file.puts "CLOSE:#{@folder}/#{@filename}\n"
										
										destFile2 = File.open(File.expand_path("../ds/#{@filename}"))
										newFileContent = destFile2.read
										endline = newFileContent.gsub("\n", " ")
										#puts "New file content sent: #{endline}"
										s_file.puts "#{endline}"
										destFile2.close
										File.delete(File.expand_path("../ds/#{@filename}"))
										
									}
																		
									client.puts( "#{@filename} closed." )
									
									
									#Returns locking key
									l_file_thread = Thread.new{
										#puts "In s_file thread"
										hostname = "localhost"
										p = 3000
										l_file = TCPSocket.open(hostname, p)
										
										l_file.puts("CLOSE:#{@filename}")
																			
									}
								
								else
									puts "Error"
									client.puts "ERROR_CODE: 1\nERROR_DESCRIPTION: Command not valid"
									#client.close	
									#Thread.kill(worker_thread)					
								end
						
						
						
						
							#Command that waits for next recv
							end
						}

					end
					rescue ThreadError
				end

			end
			
		end; "ok"	

		#Joins threads back to main thread before program exits
		workers.map(&:join); "ok"

	end
end

server = Server.new