require "socket"
require "thread"

class Server
	def initialize
		@port = 3000
	    @server = TCPServer.open( @port )
		
		@files = Hash.new
		@q = Hash.new
		
		#Initialises q variables for filenames
		@files["file1.txt"] = 1
		@q["file1.txt"] = 0
		
		run
	end
	
	def run
		#Initialises thread pool queue						
		worker_q = Queue.new

		#Listening loop	
		#loop{
		server_thread = Thread.new{

			#loop{
				puts "Listening for new clients.."
				while new_client = @server.accept
					worker_q.push new_client
				end
			#}
		}
			
				
		#Thread pool loop
		#Initialises set number of threads that can process client requests
		#While there are threads left, creates new thread to handle individual clients
		workers = (0...4).map do
		
			worker_thread = Thread.new do
				begin
					#While there are objects in the queue, 
					#the next free thread will pop a client socket off the queue 
					#to be handled 
					while client = worker_q.pop
						puts("Client being handled by thread ##{Thread.current.object_id}.")
						
						#Loop for parsing commands from client and responding
						loop {
							
							while test_string = client.gets(4)
																		
								if test_string == 'OPEN'
									
									string = client.gets(512)
									puts("Request received: #{test_string}#{string.chomp}")
									str_split = string.split(/([:]+)/)
									filename = str_split[2].chomp
									#puts filename
									
									#Allocates client a number in the queue
									@q[filename] += 1
									puts "Client number: #{@q[filename]}"
									
									#For requested filename
									#Waits for file number to be equal to queue number
									puts "File number: #{@files[filename]}"
									if @files[filename] == @q[filename]
										puts "In loop"
										client.puts "#{filename}: #{@q[filename]}"
										
									end
									
									
								elsif test_string == 'CLOS'
									
									string = client.gets(512)
									puts("Request received: #{test_string}#{string.chomp}")
									str_split = string.split(/([:]+)/)
									filename2 = str_split[2].chomp
									puts filename2
																		
									#Moves the file queue along one, 
									#unlocking file for the next client in the queue
									@files[filename2] += 1
									puts "New queue number: #{@files[filename2]}"
								
								else
									puts "Error"
									client.puts "ERROR_CODE: 1\nERROR_DESCRIPTION: Command not valid"
									#client.close	
									#Thread.kill(worker_thread)					
								end
						
						
						
						
							#Command that waits for next recv
							end
						}

					end
					rescue ThreadError
				end

			end
			
		end; "ok"	

		#Joins threads back to main thread before program exits
		workers.map(&:join); "ok"

	end
end



#Takes in port number from command line
#port = ARGV[0]

#server = Server.new(port)

server = Server.new