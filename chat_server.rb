require "socket"
require "thread"

class Server
	def initialize
		@port = 5000
	    @server = TCPServer.open( @port )
		
		@rooms = Hash.new
		#Key: room_ref, Value: room_name
		@clients = Hash.new
		#Key: client_name, Value: client connection
		@connections = Hash.new{|hash, key| hash[key] = Array.new}
		#Key: room_ref, Value: client_name
		
		@join_id = 0
		@room_ref_index = 0
		
		@joins = Hash.new
		@joins[:rooms] = @rooms
		@joins[:clients] = @clients
		@joins[:connections] = @connections
		
		@join_ids = Array.new
		
		run
	end

	def run
		#Initialises thread pool queue						
		worker_q = Queue.new

		#Listening loop	
		#loop{
		server_thread = Thread.new{

			#loop{
				puts "Listening for new clients..\n"
				while new_client = @server.accept
					worker_q.push new_client
				end
			#}
		}

		worker_count = 0
		
		#Thread pool loop
		#Initialises set number of threads that can process client requests
		#While there are threads left, creates new thread to handle individual clients
		
		workers = (0...4).map do
		
		#while worker_count < 10
			worker_thread = Thread.new do
				#worker_count +=1
				begin
					#While there are objects in the queue, 
					#the next free thread will pop a client socket off the queue 
					#to be handled 
					while client = worker_q.pop
						puts("Client being handled by thread ##{Thread.current.object_id}.\n")
						
						#Loop for parsing commands from client and responding
						loop {
							
						while test_string = client.gets(4)
					
							if test_string == 'HELO'
								string = client.gets(512)
								puts("Command received: #{test_string}#{string.chomp}\n")
								client.puts("#{test_string}#{string.chomp}\nIP:[macneill@scss.tcd.ie]\nPort:[#{@port}]\nStudentID:[10315881]")
								#Thread.kill(worker_thread)
								#worker_count -=1
								
							
							elsif test_string == 'KILL'
								string = client.gets(512)
								puts "Server being shut down."
								client.close
								exit
							
							
							elsif test_string == 'JOIN'
								room_ref = 0
								
								puts "Command received: #{test_string}"
								string = client.gets(512)
								puts string
																
								str_split = string.split(/([:]+)/)
															
								room_s = str_split[2].split(/\\n/)
								room_a = []
								room_s.each do |arg|
									room_a.push(arg)
								end							
								room_name = room_a[0]
								#puts "Room name: #{room_name}"
								
								client_name = str_split[8].chomp
								#puts "Client name: #{client_name}"													
														
								#Allocates room_ref to room
								@joins[:rooms].each do |other_room_ref, other_room_name|
									if room_name == other_room_name
										room_ref = other_room_ref
										#puts "Room occupied.\n"
									
									end
								end
								if room_ref == 0
									@room_ref_index += 1
									room_ref = @room_ref_index
									@joins[:rooms][room_ref] = room_name
									#puts "Room empty.\n"
								end
								
								
								#Stores client name and connection
								@joins[:clients][client_name] ||= []
								@joins[:clients][client_name] << client 
								#puts "Client connection: #{@joins[:clients].values_at(client_name)}"
								
								
								#Adds connection to join look up table if not in existence
								connected = 0
								@joins[:connections].each_key do |other_room|
									if other_room == room_ref
										other_names = @joins[:connections].values_at(other_room)
										if other_names.include?(client_name)
											connected = 1
										end	
									end	
								end
								
								if connected == 0
									@joins[:connections][room_ref] ||= []
									@joins[:connections][room_ref] << client_name 
									#puts "Room connection: #{@joins[:connections].values_at(room_ref)}"
								end
								
								
								#Allocates join_id to room-client pair						
								@join_id += 1					
								@join_ids.push(@join_id) 
								
								#Responds to client
								client.puts "JOINED_CHATROOM:#{room_name}\\nSERVER_IP:macneill@scss.tcd.ie\\nPORT:#{@port}\\nROOM_REF:#{room_ref}\\nJOINID:#{@join_id}"
								
								
								#Sends message to everyone in room_name that client_name has joined
								@joins[:connections].each_key do |other_room|
									if other_room == room_ref
										other_names = @joins[:connections].values_at(other_room)
										other_names.each do |other_client|
											next_clients = @joins[:clients].values_at(client_name)
											#puts "Next clients: #{next_clients}"
											next_clients.each do |next_client|
												puts "Next client: #{next_client}"
												client.puts "#{client_name} has joined chatroom #{room_ref}."
											end 
										end	
									end	
								end
							
							
							elsif test_string == 'LEAV'
								puts "Command received: #{test_string}E"
								
								string = client.gets(512)
								puts string
																														
								str = string.split(/([:]+)/)
								str_split = []
								str.each do |arg|
									str_split.push(arg)
								end
								
								line1 = str_split[2].split(/\\n/)
								room_ref = line1[0]
								
								line2 = str_split[4].split(/\\n/)
								join_id = line2[0]
								#puts "Join ID: #{join_id}"
								
								client_name = str_split[6].chomp
								#puts "Client name: #{client_name}"
														
								room_ref_int = room_ref.ord - 48
								#puts room_ref_int
								
								#Responds to client
								client.puts "LEFT_CHATROOM:#{room_ref_int.to_s}\\nJOIN_ID:#{join_id}"
								
								#Removes connection
								@joins[:connections].delete(room_ref_int)
								#c_names = @joins[:connections].values_at(room_ref_int)
								#c_names.each do |name|
									#puts "Name: #{name}"
									#if name == client_name
										#c_names.delete(name)
									#end
								#end
								
								
								#Sends message to everyone in room_name that client_name has left
								@joins[:connections].each_key do |other_room|
									if other_room == room_ref_int
										other_names = @joins[:connections].values_at(other_room)
										other_names.each do |other_client|
											puts "Other client: #{other_client}"
											next_clients = @joins[:clients].values_at(client_name)
											next_clients.each do |next_client|
												#puts "Next client: #{next_client}"
												#client.puts "LEFT_CHATROOM:#{room_ref.to_s}\nCLIENT_NAME:#{client_name.to_s}"
									
											end 
										end	
									end	
								end
																														
							
							elsif test_string == 'CHAT'
																
								puts "Command received: #{test_string}\n"
								string = client.gets(512)
								puts string
								
								str_split = string.split(/([:]+)/)
															
								room_r = str_split[2].split(/\\n/)				
								room_ref = room_r[0]
								#puts "Room ref: #{room_ref}"
								
								client_n = str_split[6].split(/\\n/)
								client_name = client_n[0]
								#puts "Client: #{client_name}"
								
								msg = str_split[8].chomp
								
								room_ref_int = room_ref.ord - 48
								#puts room_ref_int
								
								#Sends message to everyone in room_name
								@joins[:connections].each_key do |other_room|
									if other_room == room_ref_int
										other_names = @joins[:connections].values_at(other_room)
										other_names.each do |other_client|
											next_clients = @joins[:clients].values_at(client_name)
											#puts "Next clients: #{next_clients}"
											next_clients.each do |next_client|
												puts "Next client: #{next_client}"
												client.puts "CHAT:#{room_ref.to_s}\\nCLIENT_NAME:#{client_name.to_s}\\nMESSAGE:#{msg.to_s}"
									
											end 
										end	
									end	
								end
								
							
							elsif test_string == 'DISC'
								
								puts "Command received: #{test_string}ONNECT"
								#DISCONNECT: [0]PORT: [0]
								#CLIENT_NAME: [string handle to identify client user]
								string = client.gets(512)
								
								str = string.split(/\\n/)
								str_split = []
								str.each do |arg|
									str_split.push(arg)
								end
								
								line3 = str_split[2].split(/:/)
								line3_split = []
								line3.each do |arg|
									line3_split.push(arg)
								end
								client_name = line3_split[1].chomp
								
								#Removes room-client connection
								c = @joins[:connections].values_at(room_ref_int)
								c.each do |name|
									if name == client_name
										c.delete(name)
									end
								end
								
								#Disconnects specified client
								@joins[:clients].each do |other_name, other_client|
									if other_name == client_name
										@joins[:clients].delete(other_name)
										#Can be re-added for testing on multiple machines
										#other_client.close
										#Thread.kill(worker_thread)
									end
								end
								
								
							else
								puts "Error"
								client.puts "ERROR_CODE: 1\nERROR_DESCRIPTION: Command not valid"
								#client.close	
								#Thread.kill(worker_thread)					
							end
							
						#Command that waits for next recv
						end
						}

					end
					rescue ThreadError
				end

			end
			
		end; "ok"	

		#Joins threads back to main thread before program exits
		workers.map(&:join); "ok"

	end
end

#Takes in port number from command line
#port = ARGV[0]
#server = Server.new(port)

server = Server.new