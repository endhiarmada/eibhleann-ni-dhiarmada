require "socket"
require "thread"

#Takes in port number from command line
port = ARGV[0]

server = TCPServer.open(port) 
puts "Socket opened on port #{port}." 

#Initialises thread pool queue						
worker_q = Queue.new

#Listening loop	
	server_thread = Thread.new{

		loop{
			puts "Listening for new clients.."
			new_client = server.accept
			command = new_client.gets
			print("Command received: ", command)
			actual_command = command.chomp
		
			if actual_command.include? "HELO"
				#new_client.puts("#{actual_command}\nIP:[#{Socket.gethostname}]\nPort:[#{port}]\nStudentID:[10315881]")
				new_client.puts("#{actual_command}\nIP:[#{Socket.)ip_address_list}]\nPort:[#{port}]\nStudentID:[10315881]")
				new_client.close

			elsif actual_command.include? "KILL_SERVICE"
				new_client.puts("Closing connection..")
				exit

			else
				new_client.puts("Request is being transferred..")
				worker_q.push new_client
				puts "Request pushed to thread pool queue."
			end
		}
	}

#Thread pool loop
#Initialises set number of threads that can process client requests
workers = (0...5).map do
	worker_thread = Thread.new do
		begin
			#While there are objects in the queue, the next free thread will pop an client socket off the queue to be handled
			while client = worker_q.pop
				puts("Client being handled by thread ##{Thread.current.object_id}..")
			
				#Following lines taken out for testing on local machine
				#client.close
				#puts "Connection closed"
			end
		rescue ThreadError
		end

	end
	
end; "ok"	

#Joins threads back to main thread before program exits
workers.map(&:join); "ok"