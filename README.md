# Lab submissions for CS4032 #
## Student number: 10315881 ##
# Lab 2 #
Server written in Ruby, can be run by entering
.\start.sh [Port number]
from the command line.
# Lab 4 #
Chat system written in Ruby. Operation can be tested by running clientCHAT,rb, clientHELO.rb and clientKILL.rb with chat_server.rb
# Project #
Distributed file system written in Ruby composed of transparent file access client proxy with caching capabilities, file server, directory service and locking service. Operation is automatic and can be tested by running file_server.rb, directory.rb, lock.rb, proxy.rb with clientFILE.rb which sends open, close, read and write commands to the proxy. clientHELO and clientKILL can also be parsed by the proxy. As testing is automatic all source files need to be run from a folder called "ds" and folder file_server1 containing file1.txt should also be present in this folder.