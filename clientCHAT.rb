require 'socket'      # Sockets are in standard library

hostname = "localhost"
port = 5000

room_name = "roomA"
client_name = "client1"

puts "Attempting to connect.."
s = TCPSocket.open(hostname, port)

#loop{
	
	s.puts "JOIN_CHATROOM:#{room_name}\\nCLIENT_IP:0\\nPORT:0\\nCLIENT_NAME:#{client_name}"
	reply_yo = s.recv(512) 
	puts reply_yo
														
	str_split = reply_yo.split(/([:]+)/)
									
	room = str_split[8].split(/\\n/)						
	room_ref = room[0].chomp
	room_ref_int = room_ref.ord - 48
									
	id = str_split[10].chomp
	id_int = id.ord - 48

	reply_join = s.recv(512) 
	puts reply_join
	
	sleep(1) 
	
	#s.puts ("CHAT:1\\nJOIN_ID:1\\nCLIENT_NAME:#{client_name}\\nMESSAGE:Hi! How are you?\n")
	s.puts "CHAT:#{room_ref_int.to_s}\\nJOIN_ID:#{id_int.to_s}\\nCLIENT_NAME:#{client_name}\\nMESSAGE:Hi! How are you?"
	reply_chat = s.recv(512) 
	puts reply_chat		

	sleep(1)
	
	s.puts "LEAVE:#{room_ref_int.to_s}\\nJOIN_ID:#{id_int.to_s}\\nCLIENT_NAME:#{client_name}"
	reply_leave = s.recv(512) 
	puts reply_leave	

#}